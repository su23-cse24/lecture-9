#include<iostream>
using namespace std;

int main() {

    // stack = 8MB => 8,000,000 bytes
    // int = 4 bytes

    int N = 5000000;
    int* arr = new int[N];

    for (int i = 0; i < N; i++) {
        arr[i] = 0;
    }

    cout << "DONE" << endl;

    delete[] arr;

    return 0;
}