#include<iostream>
using namespace std;

int main() {

    int x = 1162692948;

    int* pi = &x;
    cout << "pi: " << (long) pi << endl;
    cout << "pi-> " << *pi << endl;

    char* pc = (char*) &x;
    cout << "pc: " << (long) pc << endl;
    cout << "pc-> " << (int) *pc << endl;

    cout << "pc: " << (long) pc + 1 << endl;
    cout << "pc-> " << (int) *(pc + 1) << endl;

    cout << "pc: " << (long) pc + 2 << endl;
    cout << "pc-> " << (int) *(pc + 2) << endl;

    cout << "pc: " << (long) pc + 3 << endl;
    cout << "pc-> " << (int) *(pc + 3) << endl;

    return 0;
}