#include<iostream>
#include<vector>
using namespace std;

int main() {

    int capacity = 1;
    int count = 0;
    int* arr = new int[capacity];

    int x;
    while (cin >> x) {
        arr[count] = x;
        count++;

        if (count == capacity) {
            capacity *= 2;

            int* temp = new int[capacity];

            for (int i = 0; i < count; i++) {
                temp[i] = arr[i];
            }

            int* old = arr;
            arr = temp;
            delete[] old;
            
        }

    }

    cout << "Capacity: " << capacity << endl;
    cout << "Count: " << count << endl;
    cout << "arr: ";
    for (int i = 0; i < count; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;

    return 0;
}