#include<iostream>
using namespace std;

void pointerArithmetic(int* p, int size) {
    cout << "start: " << (long) p << endl;

    for (int i = 0; i < size; i++) {
        cout << "p: " << (long) p << endl;
        cout << "p-> " << *p << endl;
        p++;
    }

    cout << "end: " << (long) p << endl;
}

void arraySubscripting(int* p, int size) {
    cout << "start: " << (long) p << endl;

    for (int i = 0; i < size; i++) {
        cout << "p-> " << *(p + i) << endl;
    }

    cout << "end: " << (long) p << endl;
}

int main() {

    const int size = 5;
    int arr[size] = {3, 5, 7, 9, 11};

    int* p = arr;

    // pointerArithmetic(p, size);

    arraySubscripting(p, size);


    return 0;
}